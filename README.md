Static pages for Propovednik site
=======

Prod: https://propovednik.com/  
[Admin](https://propovednik.com/admin)  

Board
=====
https://gitlab.com/propovednik/propovednik/boards?=

Permissions
=====
- `Public` access to repo (`Guest` level)
- Only project members with `Developer` can create branches, which will automatically be deployed to Staging
- Only `Owner` can promote to Prod

CI
==
MySQL settings required:  
https://dev.mysql.com/doc/refman/8.0/en/string-functions.html#function_load-file
